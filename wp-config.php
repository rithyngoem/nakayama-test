<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 * *
 * * Never forget to update the DOMAINNAME accordingly
 * *
 * * Never forget to update the $table_prefix too, should be in the format of wp_prefix_
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
 
// Set up WordPress to live in the /core folder

if( ( $_SERVER['SERVER_NAME'] == 'DOMAINNAME.com' ) || ( $_SERVER['SERVER_NAME'] == 'www.DOMAINNAME.com' )  ):
	
	define('WP_ENV', 'prod');
	
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/core');
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);
	define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content');
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');
	
elseif( $_SERVER['SERVER_NAME'] == 'staging.camwpdev.com' ):
	
	define('WP_ENV', 'staging');
	
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/SITENAME/core');
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '/SITENAME');
	define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content');
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/SITENAME/wp-content');
	
else:
	
	define('WP_ENV', 'localdev');
	
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/core');
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);
	define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content');
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');
	
endif;

  
if( WP_ENV == 'staging' ):

	define('DB_NAME', 'DBNAME');
	define('DB_USER', 'DBUSER');
	define('DB_PASSWORD', 'DBPASS');
	define('DB_HOST', 'localhost');

	define('WP_DEBUG', false);

elseif( WP_ENV == 'prod' ):

	define('DB_NAME', 'DBNAME');
	define('DB_USER', 'DBUSER');
	define('DB_PASSWORD', 'DBPASS');
	define('DB_HOST', 'localhost');

	define('WP_DEBUG', false);

elseif( WP_ENV == 'localdev' ):
  
	define('DB_NAME', 'nakayama');
	define('DB_USER', 'root');
	define('DB_PASSWORD', '');
	define('DB_HOST', 'localhost');

	define('WP_DEBUG', true);

else:

  exit('No environment.');
  
endif;

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|wZEoNK`+|F3O`N*37U}uVM~-o,%Bc`sAn(o*8PW9N]YId@?u?W3K7{)e U/CHi|');
define('SECURE_AUTH_KEY',  'umZQ{NUjG- 6 ]+*@afIXcJu tg+P]PR+KRo%PhwM/`07!fEH9xAbE@dgLU+*cW]');
define('LOGGED_IN_KEY',    'XWUp=~ebPf*Qx@&/A+nMTq:/U}LOqq5x11}0o}PPlK.;sk)bOY[tg3KkKKV&n]1x');
define('NONCE_KEY',        '|R&_|GEnrl+e)?b02z2=Cy|! :+L>jdH8ed>D+V{fL%Z9U<q(j+NvRCK=2wlD]1-');
define('AUTH_SALT',        'wi1frq1o35G%Ii4V;_D|KS?7LbNA9D,r*&0tMFs|AS>mU%U##]8m[Lan.mY<9M=w');
define('SECURE_AUTH_SALT', 'w4}oZZL/+$vAi|mx)4bl4*jU;4^3u>Gkz$4_!gQ;}A)$Y|)|kQ8+4TatuF92N|aE');
define('LOGGED_IN_SALT',   'Qne,/Ys&Iaa^LbhR^X,sG=]AEFH`p-7ani$,;Ki3T]/n9a7eA[N.X1@,={+ RwGJ');
define('NONCE_SALT',       '};SLs;qqPm.,Sp$<|?[AU27RJ;Kx+u<#Q&fAmYM-S$9#13nj^|0M U4y2]?~N(PL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_pr3fix_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */


define('DISALLOW_FILE_EDIT', true);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 *	PROJECT			:		CAMWP DEV
 *	DEVELOPER		:		CAMWP DEV Team
 *	DEVELOPER URI	:		https://camwpdev.com
 *	DATE			:		20-August-2016
 */
 
$( function() {

	var PROJECTNAME = {		
		init: function() {
			
			this.backgroundImage();
			this.pentagonImage();
			this.pentagonServiceBackgroundImage();
			this.pentagonServiceDetailBg();
		},
		
		backgroundImage: function() {
			let imageUrl = $( '.has-bg' ).attr( 'data-bg-image' );

			$( '.has-bg' ).css('background-image', 'url(' + imageUrl + ')')
		},

		pentagonImage: function() {
			let imageUrl = $( '.pentagon' ).attr( 'data-image' );

			$( '.pentagon' ).css({
				'background-image': 'url(' + imageUrl + ')' 
			});
		},

		pentagonServiceBackgroundImage: function() {
			$( '.pentagon > .pentagon-item' ).each(function() {
				let imageUrl = $( this ).attr('data-bg');

				$( this ).css({
					'background-image': 'url(' + imageUrl + ')' 
				});
			})
		},

		pentagonServiceDetailBg: function() {
			$( '.service-detail.service-items > .service-item' ).each(function() {
				let imageUrl = $( this ).find('.rounded-tag').attr('data-bg');

				$( this ).find('.rounded-tag').css({
					'background-image': 'url(' + imageUrl + ')' 
				});
			});
		}
		
	}
	
	$( document ).ready( function() {
		
		PROJECTNAME.init();
		
	});
	
});
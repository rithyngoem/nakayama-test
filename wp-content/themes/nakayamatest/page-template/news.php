<?php
/**
 * Template Name: News Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 6,
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'DESC'
        );

        $news = new WP_Query( $args );

        if ( $news->have_posts() ) :
    ?>

        <div class="page-contain whats-new">
            <div class="container">
                <div class="table-responsive">
                    <table class="table">
                        <?php while ( $news->have_posts() ) : $news->the_post(); ?>

                        <?php
                            $categories = get_the_category();

                            if ( ! empty( $categories ) ) :
                        ?>
                            <tr>
                                <td>
                                    <span class="notice"><?php echo esc_html( $categories[0]->name ); ?></span>
                                </td>
                                <td><?php echo get_the_date( 'yy.m.d' ); ?></td>
                                <td><?php the_title(); ?></td>
                            </tr>

                        <?php endif; endwhile; ?>

                        <?php echo bootstrap_pagination(); ?>
                    </table>
                </div>
            </div>
        </div>

    <?php endif; wp_reset_query(); ?>
<?php
get_footer();

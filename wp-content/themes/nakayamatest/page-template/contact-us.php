<?php
/**
 * Template Name: Contact Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <div class="page-contain contact-us">
        <div class="container">
            <div class="contact-info text-center mb-5">
                <h3 class="title d-inline with-border"><img src="<?php echo get_template_directory_uri(); ?>./images/tel-icon.png" width="50" alt=""> <?php the_field( 'contact_by_phone_and_fax_text' ); ?></h3>
                <p class="mt-3 mb-3"><?php the_field( 'contact_tagline' ); ?></p>

                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <div class="direct-contact row">
                            <div class="col-md-6 text-left d-flex">
                                <img src="<?php echo get_template_directory_uri(); ?>./images/tel-with-text.png" alt="">
                                <div class="tel">
                                    <h4><?php the_field( 'contact_01' ); ?></h4>
                                    <p><?php the_field( 'contact_01_reception_hours' ); ?></p>
                                </div>
                            </div>

                            <div class="col-md-6 text-left d-flex">
                                <img src="<?php echo get_template_directory_uri(); ?>./images/fax-with-text.png" alt="">
                                <div class="tel">
                                    <h4><?php the_field( 'contact_02' ); ?></h4>
                                    <p><?php the_field( 'contact_02_reception_hours' ); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <h3 class="title d-inline with-border"><img src="<?php echo get_template_directory_uri(); ?>./images/mail.png" width="50" alt=""> <?php the_field( 'contact_by_email_text' ); ?></h3>
            </div>

            <div class="contact-form">
                <div class="contact-form-wrapper">
                    <div class="note">
                        <p><?php the_field( 'form_instruction' ); ?></p>

                        <p class="mb-3 mt-3"><span class="required">必須</span> 印は必須項目です。</p>
                    </div>

                    <?php echo do_shortcode( '[ninja_form id=1]' ); ?>
                </div>
            </div>

            <div class="term-conditions" id="term-and-condition">
                <h3 class="title with-border"><?php the_field( 'privacy_policy_section_heading' ); ?></h3>

                <?php the_field( 'privacy_policy' ); ?>
            </div>
        </div>
    </div>
<?php
get_footer();

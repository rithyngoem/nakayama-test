<?php
/**
 * Template Name: Contract Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <?php
        if ( have_rows( 'hero_section' ) ) :

        while( have_rows( 'hero_section' ) ) : the_row();
    ?>

        <div class="page-hero d-flex align-items-center has-bg" data-bg-image="<?php the_sub_field( 'background_image' ); ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content text-center">
                            <h2><?php the_sub_field( 'hero_descriptions' ); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; endif; ?>

    <div class="page-contain">
        <?php
            if ( have_rows( 'why_choose_us_section' ) ) :

            while( have_rows( 'why_choose_us_section' ) ) : the_row();
        ?>

            <div class="why-choose-us">
                <div class="section-heading">
                    <div class="container">
                        <h3 class="text-center"><?php the_sub_field( 'section_heading' ); ?></h3>
                    </div>
                </div>

                <div class="container">
                    <div class="content-wrapper">
                        <div class="list-view light-list-view">

                            <?php
                                if ( have_rows( 'contents' ) ) :

                                while( have_rows( 'contents' ) ) : the_row();
                            ?>

                                <div class="list-view-item">
                                    <h3><?php the_sub_field( 'content_title' ); ?></h3>
                                    <div class="content">
                                        <p><?php the_sub_field( 'descriptions' ); ?></p>
                                    </div>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; endif; ?>

        <?php
            if ( have_rows( 'staff_system_section' ) ) :

            while( have_rows( 'staff_system_section' ) ) : the_row();
        ?>

            <div class="staff-system">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <h3 class="left-triangle-dot"><?php the_sub_field( 'section_heading' ); ?></h3>
                        </div>
                        <div class="col-sm-6">
                            <img class="img-fluid mb-3 mb-sm-0" src="<?php the_sub_field( 'staff_system_image' ); ?>" alt="<?php the_title(); ?>">
                        </div>
                        <div class="col-sm-6">
                            <div class="staff-system-detail">

                                <?php
                                    if ( have_rows( 'staff_system_details' ) ) :

                                    while( have_rows( 'staff_system_details' ) ) : the_row();
                                ?>

                                    <h4 class="border-bottom-1"><?php the_sub_field( 'system_title' ); ?></h4>
                                    <p><?php the_sub_field( 'system_descriptions' ); ?></p>

                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; endif; ?>

        <?php
            if ( have_rows( 'flowchart_section' ) ) :

            while( have_rows( 'flowchart_section' ) ) : the_row();
        ?>

            <div class="flowchart">
                <div class="section-heading">
                    <div class="container">
                        <h3 class="text-center"><?php the_sub_field( 'section_heading' ); ?></h3>
                    </div>
                </div>

                <?php
                    $args = array(
                        'post_type' => 'flowchart',
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                        'orderby' => 'post_date',
                        'order' => 'DESC'
                    );

                    $flowchart = new WP_Query( $args );

                    if ( $flowchart->have_posts() ) :
                ?>

                    <div class="container">
                        <div class="flowchart-content">

                            <?php
                                while ( $flowchart->have_posts() ) : $flowchart->the_post();

                                $terms = get_terms( array(
                                    'taxonomy' => 'flowchart_categories',
                                    'hide_empty' => false,
                                ) );
                            ?>

                                <div class="row flowchart-item">
                                    <div class="col-md-4 flowchart-name">
                                        <h4><strong><?php echo esc_html( $terms[0]->name ); ?></strong></h4>
                                    </div>
                                    <div class="col-md-8 flowchart-description">
                                        <p><?php the_title(); ?></p>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    </div>

                <?php endif; wp_reset_query(); ?>
            </div>

        <?php endwhile; endif; ?>

        <?php
            if ( have_rows( 'outsourcing_section' ) ) :

            while( have_rows( 'outsourcing_section' ) ) : the_row();
        ?>

            <div class="outsourcing">
                <div class="section-heading">
                    <div class="container">
                        <h3 class="text-center"><?php the_sub_field( 'section_heading' ); ?></h3>
                    </div>
                </div>

                <div class="container">
                    <div class="row">

                        <div class="col-12">
                            <h4 class="text-title text-center"><?php the_sub_field( 'section_tagline' ); ?></h4>
                        </div>

                        <div class="col-md-6">
                            <p><?php the_sub_field( 'section_descriptions' ); ?></p>

                            <div class="space-divider"></div>

                            <h3 class="left-triangle-dot"><?php the_sub_field( 'content_title' ); ?></h3>

                            <?php the_sub_field( 'descriptions' ); ?>

                            <div class="space-divider"></div>

                            <p><?php the_sub_field( 'about_text' ); ?></p>
                            <p><?php the_sub_field( 'address' ); ?></p>
                        </div>

                        <div class="col-md-6">

                            <?php
                                if ( have_rows( 'images' ) ) :

                                while( have_rows( 'images' ) ) : the_row();
                            ?>
                                <img class="img-fluid" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_title(); ?>">

                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; endif; ?>
    </div>
<?php
get_footer();

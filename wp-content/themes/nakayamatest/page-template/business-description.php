<?php
/**
 * Template Name: Business Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <?php
        $args = array(
            'post_type' => 'business',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'ASC'
        );

        $business = new WP_Query( $args );

        if ( $business->have_posts() ) :
    ?>

        <div class="page-contain">
            <div class="container">
                <div class="about-item-wrapper">

                    <?php while ( $business->have_posts() ) : $business->the_post(); ?>

                        <div class="about-item">
                            <div class="row">
                                <div class="col-lg-8">
                                    <h4 class="title with-border"><?php the_title(); ?></h4>
                                </div>
                            </div>

                            <div class="about-content">
                                <div class="row">
                                    <div class="col-lg-8">

                                        <?php
                                            if ( have_rows( 'business_items' ) ) :

                                            while( have_rows( 'business_items' ) ) : the_row();
                                        ?>

                                            <strong><?php the_sub_field( 'business_item_heading' ); ?></strong>
                                            <p><?php the_sub_field( 'business_item_description' ); ?></p>

                                        <?php endwhile; endif; ?>
                                    </div>
                                    <div class="col-lg-4">

                                        <?php
                                            if ( have_rows( 'Images' ) ) :

                                            while( have_rows( 'Images' ) ) : the_row();
                                        ?>

                                            <img src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_title(); ?>">

                                        <?php endwhile; endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        </div>

    <?php endif; wp_reset_query(); ?>
<?php
get_footer();

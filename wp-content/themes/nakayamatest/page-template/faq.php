<?php
/**
 * Template Name: FAQ Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <?php
        if ( have_rows( 'hero_section' ) ) :

        while( have_rows( 'hero_section' ) ) : the_row();
    ?>

        <div class="page-hero d-flex align-items-center has-bg" data-bg-image="<?php echo get_template_directory_uri(); ?>/images/line-bg.jpg">
            <div class="container">
                <img class="img-fluid" src="<?php the_sub_field( 'background_image' ); ?>" alt="<?php the_title(); ?>">
            </div>
        </div>

    <?php endwhile; endif; ?>

    <?php
        $args = array(
            'post_type' => 'faq',
            'posts_per_page' => 6,
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'DESC'
        );

        $faqs = new WP_Query( $args );

        if ( $faqs->have_posts() ) :
    ?>

    <div class="page-contain">
        <div class="container">
            <div class="list-view light-list-view">

                <?php while ( $faqs->have_posts() ) : $faqs->the_post(); ?>

                    <div class="list-view-item">
                        <h3><?php the_title(); ?></h3>
                        <div class="content">
                            <div class="row">

                                <?php
                                    if ( have_rows( 'questions_&_answers' ) ) :

                                    while( have_rows( 'questions_&_answers' ) ) : the_row();
                                ?>

                                    <div class="col-md-6">
                                        <div class="card faq-card">
                                            <div class="card-header d-flex align-items-center">
                                                <h4 class="text-white"><?php the_sub_field( 'question' ); ?></h4>
                                            </div>
                                            <div class="card-body d-flex">
                                                <p><?php the_sub_field( 'answers' ); ?></p>
                                            </div>
                                        </div>
                                    </div>

                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>
            </div>
        </div>
    </div>

    <?php endif; wp_reset_query(); ?>
<?php
get_footer();

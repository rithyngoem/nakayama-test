<?php
/**
 * Template Name: Company Profile Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <div class="page-contain">
        <div class="container">
            <div class="general-info">

                <?php
                    if ( have_rows( 'management_section' ) ) :

                    while( have_rows( 'management_section' ) ) : the_row();
                ?>

                    <div class="general-info-item">
                        <div class="title-bordered">
                            <h3><?php the_sub_field( 'section_heading' ); ?></h3>
                        </div>

                        <h4 class="main-description"><?php the_sub_field( 'tagline' ); ?></h4>

                        <div class="row">

                            <?php
                                if ( have_rows( 'q&a' ) ) :

                                while( have_rows( 'q&a' ) ) : the_row();
                            ?>

                                <div class="col-md-2 dots-end-md">
                                    <strong><?php the_sub_field( 'question' ); ?></strong>
                                </div>
                                <div class="col-md-10">
                                    <ul class="list-inline square-list-style">

                                        <?php
                                            if ( have_rows( 'answers_list' ) ) :

                                            while( have_rows( 'answers_list' ) ) : the_row();
                                        ?>

                                            <li class="list-inline-item"><?php the_sub_field( 'answer' ); ?></li>

                                        <?php endwhile; endif; ?>
                                    </ul>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>
                    </div>

                <?php endwhile; endif; ?>

                <?php
                    if ( have_rows( 'action_pointer_section' ) ) :

                    while( have_rows( 'action_pointer_section' ) ) : the_row();
                ?>

                    <div class="general-info-item">
                        <div class="title-bordered">
                            <h3><?php the_sub_field( 'section_heading' ); ?></h3>
                        </div>

                        <div class="row">

                            <?php
                                if ( have_rows( 'q&a' ) ) :

                                while( have_rows( 'q&a' ) ) : the_row();
                            ?>
                                <div class="col-md-2 dots-end-md">
                                    <strong><?php the_sub_field( 'question' ); ?></strong>
                                </div>

                                <div class="col-md-10">
                                    <ul class="list-inline square-list-style">
                                        <?php
                                            if ( have_rows( 'answers_list' ) ) :

                                            while( have_rows( 'answers_list' ) ) : the_row();
                                        ?>

                                            <li class="list-inline-item"><?php the_sub_field( 'answer' ); ?></li>

                                        <?php endwhile; endif; ?>
                                    </ul>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>
                    </div>

                <?php endwhile; endif; ?>
            </div>

            <div class="about-company">
                <div class="row">

                    <?php
                        if ( have_rows( 'general_info_section' ) ) :

                        while( have_rows( 'general_info_section' ) ) : the_row();
                    ?>

                        <div class="col-md-8">

                            <?php
                                if ( have_rows( 'about_us' ) ) :

                                while( have_rows( 'about_us' ) ) : the_row();
                            ?>

                                <h3 class="title"><?php the_sub_field( 'about_heading' ); ?></h3>

                                <div class="table-responsive mb-5">
                                    <table class="table">
                                        <tr>
                                            <th scope="col">会社名</th>
                                            <td><?php the_sub_field( 'company_name' ); ?></td>
                                        </tr>
                                        <tr>
                                            <th scope="col">代表者名</th>
                                            <td><?php the_sub_field( 'representative_name' ); ?></td>
                                        </tr>
                                        <tr>
                                            <th scope="col">会社名</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'about_company' ) ) :

                                                    while( have_rows( 'about_company' ) ) : the_row();
                                                ?>

                                                    <p><?php the_sub_field( 'about_text_01' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p><?php the_sub_field( 'about_text_02' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">資本金</th>
                                            <td><?php the_sub_field( 'capital' ); ?></td>
                                        </tr>
                                        <tr>
                                            <th scope="col">設立</th>
                                            <td><?php the_sub_field( 'established' ); ?></td>
                                        </tr>
                                        <tr>
                                            <th scope="col">従業員数</th>
                                            <td><?php the_sub_field( 'number_of_employees' ); ?></td>
                                        </tr>
                                    </table>
                                </div>

                            <?php endwhile; endif; ?>

                            <?php
                                if ( have_rows( 'office_locations' ) ) :

                                while( have_rows( 'office_locations' ) ) : the_row();
                            ?>

                                <h3 class="title"><?php the_sub_field( 'office_location_heading' ); ?></h3>

                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th scope="col">本社</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'head_office' ) ) :

                                                    while( have_rows( 'head_office' ) ) : the_row();
                                                ?>

                                                    <p><?php the_sub_field( 'address' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p>TEL：<?php the_sub_field( 'mobile_phone' ); ?>　FAX：<?php the_sub_field( 'fax' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">小牧オフィス</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'komaki_office' ) ) :

                                                    while( have_rows( 'komaki_office' ) ) : the_row();
                                                ?>

                                                    <p><?php the_sub_field( 'address' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p>TEL：<?php the_sub_field( 'mobile_phone' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">江南今市場工場</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'gangnam_ima_market_factory' ) ) :

                                                    while( have_rows( 'gangnam_ima_market_factory' ) ) : the_row();
                                                ?>

                                                    <p><?php the_sub_field( 'address' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p>TEL：<?php the_sub_field( 'mobile_phone' ); ?>　FAX：<?php the_sub_field( 'fax' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">小牧村中工場</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'komaki_village_naka_factory' ) ) :

                                                    while( have_rows( 'komaki_village_naka_factory' ) ) : the_row();
                                                ?>
                                                    <p><?php the_sub_field( 'address' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p>TEL：<?php the_sub_field( 'mobile_phone' ); ?>　FAX：<?php the_sub_field( 'fax' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">フィリピン工場</th>
                                            <td>
                                                <?php
                                                    if ( have_rows( 'philippines_factory' ) ) :

                                                    while( have_rows( 'philippines_factory' ) ) : the_row();
                                                ?>

                                                    <p><?php the_sub_field( 'address_1' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p><?php the_sub_field( 'address_2' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p><?php the_sub_field( 'address_3' ); ?></p>
                                                    <div class="dashed-divider"></div>
                                                    <p><?php the_sub_field( 'address_4' ); ?></p>

                                                <?php endwhile; endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">従業員数</th>
                                            <td><?php the_sub_field( 'number_of_employees' ); ?></td>
                                        </tr>
                                    </table>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>

                    <?php endwhile; endif; ?>

                    <div class="col-md-4">
                        <div class="company-image row">

                            <?php
                                if ( have_rows( 'images' ) ) :

                                while( have_rows( 'images' ) ) : the_row();
                            ?>

                                <div class="col-md-12 col-3 company-image-item mb-3 text-md-left text-center">
                                    <img src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                                    <strong class="company-image-title"><?php the_sub_field( 'image_descriptions' ); ?></strong>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">

                        <?php
                            if ( have_rows( 'about_company_section' ) ) :

                            while( have_rows( 'about_company_section' ) ) : the_row();
                        ?>

                            <h3 class="title"><?php the_sub_field( 'section_heading' ); ?></h3>

                            <div class="table-responsive mb-5">
                                <table class="table">

                                    <?php
                                        if ( have_rows( 'histories' ) ) :

                                        while( have_rows( 'histories' ) ) : the_row();
                                    ?>
                                        <tr>
                                            <th scope="col"><?php the_sub_field( 'year' ); ?></th>
                                            <td><?php the_sub_field( 'description' ); ?></td>
                                        </tr>

                                    <?php endwhile; endif; ?>
                                </table>
                            </div>

                        <?php endwhile; endif; ?>

                        <?php
                            if ( have_rows( 'bank_section' ) ) :

                            while( have_rows( 'bank_section' ) ) : the_row();
                        ?>

                            <h3 class="title"><?php the_sub_field( 'section_heading' ); ?></h3>

                            <div class="table-responsive mb-5">
                                <table class="table">
                                    <tr>
                                        <td><?php the_sub_field( 'descriptions' ); ?></td>
                                    </tr>
                                </table>
                            </div>

                        <?php endwhile; endif; ?>

                        <?php
                            if ( have_rows( 'bank_section' ) ) :

                            while( have_rows( 'bank_section' ) ) : the_row();
                        ?>

                            <h3 class="title"><?php the_sub_field( 'section_heading' ); ?></h3>

                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td><?php the_sub_field( 'descriptions' ); ?></td>
                                    </tr>
                                </table>
                            </div>

                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
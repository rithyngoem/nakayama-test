<?php
/**
 * Template Name: Visual Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama
 */

get_header();
?>

    <?php get_template_part( 'template-parts/content', 'breadcrumb' ); ?>

    <?php
        if ( have_rows( 'hero_section' ) ) :

        while( have_rows( 'hero_section' ) ) : the_row();
    ?>

        <div class="page-hero bg-img-right d-flex align-items-center has-bg" data-bg-image="<?php echo get_template_directory_uri(); ?>/images/triagle-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="title with-border"><?php the_sub_field( 'section_heading' ); ?></h4>

                        <img class="img-fluid mobile-only" src="<?php the_sub_field( 'background_image' ); ?>" alt="<?php the_title(); ?>">

                        <div class="content">
                            <strong><?php the_sub_field( 'content_heading' ); ?></strong>
                            <p><?php the_sub_field( 'content_descriptions' ); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; endif; ?>

    <div class="page-contain">
        <div class="container">
            <div class="row">

                <?php
                    if ( have_rows( 'content_with_content_heading' ) ) :

                    while( have_rows( 'content_with_content_heading' ) ) : the_row();
                ?>

                    <div class="col-md-6">
                        <h4 class="title with-border"><?php the_sub_field( 'content_title' ); ?></h4>

                        <?php
                            if ( have_rows( 'descriptions' ) ) :

                            while( have_rows( 'descriptions' ) ) : the_row();
                        ?>

                            <div class="content">
                                <strong><?php the_sub_field( 'content_heading' ); ?></strong>
                                <p><?php the_sub_field( 'descriptions' ); ?></p>
                            </div>

                        <?php endwhile; endif; ?>
                    </div>

                <?php endwhile; endif; ?>

                <div class="col-md-6">
                    <?php
                        if ( have_rows( 'content_without_content_heading' ) ) :

                        while( have_rows( 'content_without_content_heading' ) ) : the_row();
                    ?>

                        <h4 class="title with-border"><?php the_sub_field( 'content_title' ); ?></h4>
                        <div class="content">
                            <?php the_sub_field( 'descriptions' ); ?>
                        </div>

                    <?php endwhile; endif; ?>
                </div>
            </div>

        <?php
            if ( have_rows( 'inspection_assets_section' ) ) :

            while( have_rows( 'inspection_assets_section' ) ) : the_row();
        ?>

            <div class="inspection-assets">
                <div class="container">
                    <div class="section-title">
                        <h4 class="title with-border"><?php the_sub_field( 'content_title' ); ?></h4>
                    </div>

                    <div class="section-body">

                        <?php
                            if ( have_rows( 'content_descriptions' ) ) :

                            while( have_rows( 'content_descriptions' ) ) : the_row();
                        ?>

                            <div class="inspection-assets-item with-divider">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="content">
                                            <h3 class="font-weight-bold"><?php the_sub_field( 'content_heading' ); ?></h3>
                                            <p><?php the_sub_field( 'content_descriptions' ); ?></p>
                                        </div>
                                    </div>

                                    <?php
                                        if ( have_rows( 'images' ) ) :

                                        while( have_rows( 'images' ) ) : the_row();
                                    ?>
                                        
                                        <div class="col-sm-4">
                                            <img class="img-fluid mb-2" src="<?php the_sub_field( 'image' ); ?>" alt="<?php the_title(); ?>">
                                            <ul class="list-group list-group-horizontal img-detail">
                                                <?php the_sub_field( 'image_description' ); ?>
                                            </ul>
                                        </div>

                                    <?php endwhile; endif; ?>
                                </div>
                            </div>

                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>

        <?php endwhile; endif; ?>
    </div>
<?php
get_footer();

<?php
/**
 * Template Name: Homepage Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nakayama-Test
 */

get_header();
?>

    <?php
        if ( have_rows( 'hero_section' ) ) :

        while( have_rows( 'hero_section' ) ) : the_row();
    ?>

        <section class="hero d-flex align-items-center has-bg" data-bg-image="<?php the_sub_field( 'hero' ); ?>">
            <div class="container hero-content text-center">
                <h2 class="tagline"><?php the_sub_field( 'descriptions' ); ?></h2>
                <h1><?php the_sub_field( 'important_words' ); ?></h1>
            </div>
        </section>

    <?php endwhile; endif; ?>

    <?php
        if ( have_rows( 'service_section' ) ) :

        while( have_rows( 'service_section' ) ) : the_row();
    ?>

        <section class="service">
            <div class="service-header">
                <div class="container text-center">
                    <h3 class="heading"><?php the_sub_field( 'service_heading' ); ?></h3>
                </div>
            </div>
            <div class="container">
                <div class="section-heading section-customer text-center">
                    <h3><?php the_sub_field( 'section_heading' ); ?></h3>
                </div>

                <div class="service-content">
                    <div class="pentagon-service">
                        <div class="row align-items-center">

                            <?php
                                if ( have_rows( 'service_items_diagram' ) ) :

                                while( have_rows( 'service_items_diagram' ) ) : the_row();
                            ?>

                                <div class="col-md-6">
                                    <div class="pentagon-wrapper d-flex justify-content-center">
                                        <div class="pentagon" data-image="<?php the_sub_field( 'nakayama_logo' ); ?>">
                                            <div class="pentagon-item capacity rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'capacity_image' ); ?>">
                                                <h3><?php the_sub_field( 'capacity_text' ); ?></h3>
                                            </div>

                                            <div class="pentagon-item quality rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'quality_image' ); ?>">
                                                <h3><?php the_sub_field( 'quality_text' ); ?></h3>
                                            </div>

                                            <div class="pentagon-item technical-capabilities rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'technical_capability_image' ); ?>">
                                                <h3><?php the_sub_field( 'technical_capability_text' ); ?></h3>
                                            </div>


                                            <div class="pentagon-item flexibility rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'flexibility_image' ); ?>">
                                                <h3><?php the_sub_field( 'flexibility_text' ); ?></h3>
                                            </div>

                                            <div class="pentagon-item responsibility rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'responsibility_image' ); ?>">
                                                <h3><?php the_sub_field( 'responsibility_text' ); ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; endif; ?>

                            <?php
                                if ( have_rows( 'service_items_list' ) ) :

                                while( have_rows( 'service_items_list' ) ) : the_row();
                            ?>

                                <div class="col-md-6 service-detail service-items">
                                    <div class="service-item capacity d-flex align-items-center">
                                        <div class="rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'capacity_image' ); ?>">
                                            <h3 ><?php the_sub_field( 'capacity_text' ); ?></h3>
                                        </div>

                                        <div class="service-title d-flex align-items-center">
                                            <h3><?php the_sub_field( 'capacity_description' ); ?></h3>
                                        </div>
                                    </div>
                                    <div class="service-item quality d-flex align-items-center">
                                        <div class="rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'quality_image' ); ?>">
                                            <h3 ><?php the_sub_field( 'quality_text' ); ?></h3>
                                        </div>

                                        <div class="service-title d-flex align-items-center">
                                            <h3><?php the_sub_field( 'quality_description' ); ?></h3>
                                        </div>
                                    </div>
                                    <div class="service-item technical-capabilities d-flex align-items-center">
                                        <div class="rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'technical_capability_image' ); ?>">
                                            <h3><?php the_sub_field( 'technical_capability_text' ); ?></h3>
                                        </div>

                                        <div class="service-title d-flex align-items-center">
                                            <h3><?php the_sub_field( 'technical_capability_description' ); ?></h3>
                                        </div>
                                    </div>
                                    <div class="service-item flexibility d-flex align-items-center">
                                        <div class="rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'flexibility_image' ); ?>">
                                            <h3><?php the_sub_field( 'flexibility_text' ); ?></h3>
                                        </div>

                                        <div class="service-title d-flex align-items-center">
                                            <h3><?php the_sub_field( 'flexibility_description' ); ?></h3>
                                        </div>
                                    </div>
                                    <div class="service-item responsibility d-flex align-items-center">
                                        <div class="rounded-tag d-flex align-items-center justify-content-center" data-bg="<?php the_sub_field( 'responsibility_image' ); ?>">
                                            <h3><?php the_sub_field( 'responsibility_text' ); ?></h3>
                                        </div>

                                        <div class="service-title d-flex align-items-center">
                                            <h3><?php the_sub_field( 'responsibility_description' ); ?></h3>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; endif; ?>

    <?php
        if ( have_rows( 'about_section' ) ) :

        $count = 1;
    ?>

        <section class="about-section">
            <div class="container-fluid p-0">
                <?php
                    while( have_rows( 'about_section' ) ) : the_row();

                    $image_position = get_sub_field( 'image_position' );
                ?>

                    <div class="about-item <?php echo $image_position == 'Left' ? 'right-content' : 'left-content' ?> d-flex align-items-center <?php echo $count == 1 ? '' : 'cyan-bg' ?>" data-bg-image="<?php the_sub_field( 'image' ); ?>">
                        <div class="container">
                            <div class="about-content">
                                <h3><?php the_sub_field( 'title' ); ?></h3>
                                <p><?php the_sub_field( 'descriptions' ); ?></p>
                            </div>
                        </div>
                    </div>

                <?php $count++; endwhile; ?>
            </div>
        </section>

    <?php endif; ?>

    <?php
        if ( have_rows( 'inquiries_section' ) ) :

        while( have_rows( 'inquiries_section' ) ) : the_row();
    ?>

        <section class="provide-section">
            <div class="text-center">
                <h4><?php the_sub_field( 'inquiries_descriptions' ); ?></h4>
                <a href="<?php echo get_the_permalink(get_sub_field( 'inquiries_url' )); ?>" class="btn btn-outline-primary"><?php the_sub_field( 'inquiries' ); ?></a>
            </div>
        </section>

        <div class="container section-wrapper text-center">
            <div class="btn-bg-wrapper">
                <a href="<?php echo get_the_permalink(get_sub_field( 'read_more_url' )); ?>" class="btn-bg-primary"><?php the_sub_field( 'read_more' ); ?></a>
            </div>
        </div>

    <?php endwhile; endif; ?>

    <?php
        if ( have_rows( 'contract_section' ) ) :

        while( have_rows( 'contract_section' ) ) : the_row();
    ?>

        <section class="contract d-flex align-items-center has-bg" data-bg-image="<?php the_sub_field( 'background_image' ); ?>">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h3><?php the_sub_field( 'section_heading' ); ?></h3>
                    </div>
                    <div class="card-body">
                        <div class="contract-content">
                            <h4><?php the_sub_field( 'card_heading' ); ?></h4>
                            <p class="description"><?php the_sub_field( 'tagline' ); ?></p>

                            <div class="detail">
                                <p><?php the_sub_field( 'card_content_title' ); ?></p>

                                <?php the_sub_field( 'card_content' ); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contract-footer text-center">
                    <div class="btn-bg-wrapper">
                        <a href="<?php echo get_the_permalink(get_sub_field( 'read_more_url' )); ?>" class="btn-bg-primary"><?php the_sub_field( 'read_more' ); ?></a>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; endif; ?>

    <?php
        if ( have_rows( 'news_section' ) ) :

        while( have_rows( 'news_section' ) ) : the_row();
    ?>

        <section class="whats-new">
            <div class="section-heading">
                <div class="container with-right-button">
                    <h3 class="text-center"><?php the_sub_field( 'section_heading' ); ?></h3>
                    <a href="<?php echo get_the_permalink(get_sub_field( 'button_url' )); ?>" class="btn btn-outline-light"><?php the_sub_field( 'button' ); ?></a>
                </div>
            </div>

            <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => get_sub_field( 'number_of_posts' ),
                    'post_status' => 'publish',
                    'orderby' => 'post_date',
                    'order' => 'DESC'
                );

                $news = new WP_Query( $args );

                if ( $news->have_posts() ) :
            ?>

                <div class="container">
                    <div class="table-responsive">
                        <table class="table">
                            <?php
                                while ( $news->have_posts() ) : $news->the_post();

                                $categories = get_the_category();

                                if ( !empty( $categories ) ) :
                            ?>
                                <tr>
                                    <td width="90">
                                        <span class="notice"><?php echo esc_html( $categories[0]->name ); ?></span>
                                    </td>
                                    <td width="80"><?php echo get_the_date( 'yy.m.d' ); ?></td>
                                    <td width="200"><?php the_title(); ?></td>
                                </tr>

                            <?php endif; endwhile; ?>
                        </table>
                    </div>
                </div>

            <?php endif; wp_reset_query(); ?>
        </section>

    <?php endwhile; endif; ?>
<?php
get_footer();

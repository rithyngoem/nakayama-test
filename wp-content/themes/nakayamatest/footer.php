<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nakayama-Test
 */

global $redux_option;
?>
            <footer>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 text-white">
                            <div class="company-info">
                                <p><?php echo $redux_option['slogan_title_footer']; ?></p>
                                <p><?php echo $redux_option['slogan_header_footer']; ?></p>
                            </div>

                            <?php if ( $redux_option['footer_logo']['url'] != null ) : ?>
                                <img class="logo img-fluid" src="<?php echo $redux_option['footer_logo']['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
                            <?php endif; ?>

                            <address><?php echo $redux_option['address']; ?></address>

                            <p class="copy-right"><?php echo $redux_option['copyright_text']; ?></p>
                        </div>
                        <div class="col-lg-6 text-md-right text-center">
                            <div class="btn-inquiery-wrapper">
                                <a href="<?php echo $redux_option['contact_url']; ?>" class="btn btn-outline-info footer-btn-inquiery"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $redux_option['contact_btn']; ?></a>
                            </div>

                            <p class="tel">TEL <?php echo $redux_option['telephone']; ?> FAX <?php echo $redux_option['fax_number']; ?></p>
                        </div>
                    </div>
                </div>
            </footer>

        <?php wp_footer(); ?>
    </body>
</html>

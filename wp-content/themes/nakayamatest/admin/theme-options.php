<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "redux_option";


/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns      = array();

if ( is_dir( $sample_patterns_path ) ) {

    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
        $sample_patterns = array();

        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                $name              = explode( '.', $sample_patterns_file );
                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/*
 *
 * --> Action hook examples
 *
 */

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );

// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
//add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');


/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'             => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'         => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version'      => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type'            => 'submenu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'       => true,
    // Show the sections below the admin menu item or not
    'menu_title'           => __( 'Theme Options', 'redux-framework-demo' ),
    'page_title'           => __( 'Theme Options', 'redux-framework-demo' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key'       => 'AIzaSyBCNNmid7eOngJUTGogTM9pd_O_SJUOSJE',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography'     => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar'            => false,
    // Show the panel pages on the admin bar
    'admin_bar_icon'       => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority'   => 50,
    // Choose an priority for the admin bar menu
    'global_variable'      => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode'             => false,
    // Show the time the page took to load, etc
    'update_notice'        => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer'           => false,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority'        => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'          => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'     => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon'            => '',
    // Specify a custom URL to an icon
    'last_tab'             => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon'            => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug'            => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults'        => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show'         => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark'         => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export'   => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'           => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database'             => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

    // HINTS
    'hints'                => array(
        'icon'          => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color'    => 'lightgray',
        'icon_size'     => 'normal',
        'tip_style'     => array(
            'color'   => 'red',
            'shadow'  => true,
            'rounded' => false,
            'style'   => '',
        ),
        'tip_position'  => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect'    => array(
            'show' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'mouseover',
            ),
            'hide' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'click mouseleave',
            ),
        ),
    )
);


$args['share_icons'][] = array(
    'url'   => 'https://www.facebook.com/ThemesIndep',
    'title' => 'Like us on Facebook',
    'icon'  => 'el el-facebook'
);
$args['share_icons'][] = array(
    'url'   => 'https://twitter.com/themesindep',
    'title' => 'Follow us on Twitter',
    'icon'  => 'el el-twitter'
);

// Panel Intro text -> before the form
/*
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );
    */

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
$tabs = array(
    array(
        'id'      => 'redux-help-tab-1',
        'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
    ),
    array(
        'id'      => 'redux-help-tab-2',
        'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
    )
);
Redux::setHelpTab( $opt_name, $tabs );
// Set the help sidebar
$content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
Redux::setHelpSidebar( $opt_name, $content );
*/



/**
 * ---> DECLARATION OF SECTIONS
 **/

// ---> Header
Redux::setSection( $opt_name, array(
    'icon'      => 'el-icon-eye-open',
    'title'     => __('Header', 'redux-framework-demo'),
    'heading'   => __('Site Header Options', 'redux-framework-demo'),
    'fields'    => array(

        /* Main Logo Area */
        array(
            'id'        => 'site_logo',
            'type'      => 'media',
            'title'     => __('Main Logo', 'redux-framework-demo'),
            'subtitle'  => __('Upload your site logo. Default logo will be used unless you upload your own', 'redux-framework-demo'),
            'default'   => array(
                'url'   => get_template_directory_uri() .'/images/logo.png',
                'width' => '367',
                'height' => '66',
            )
        ),
        array(
            'id'        => 'footer_logo',
            'type'      => 'media',
            'title'     => __('Footer Logo', 'redux-framework-demo'),
            'subtitle'  => __('Upload logo footer. Default logo will be used unless you upload your own', 'redux-framework-demo'),
            'default'   => array(
                'url'   => get_template_directory_uri() .'/images/logo.png',
                'width' => '367',
                'height' => '66',
            )
        ),
        array(
            'id'        => 'slogan_title_header',
            'type'      => 'text',
            'title'     => __('Website Slogan Title for Header', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'slogan_header',
            'type'      => 'text',
            'title'     => __('Website Slogan for Header', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'contact_info',
            'type'      => 'text',
            'title'     => __('Contact Info Button', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'contact_info_url',
            'type'      => 'text',
            'title'     => __('Contact Info URL', 'redux-framework-demo'),
            'default'   => false
        ),
    )
) );
// ---> General Settings
Redux::setSection( $opt_name, array(
    'title'  => __('General Settings', 'redux-framework-demo'),
    'icon'   => 'el-icon-cogs',
    'fields' => array(
        array(
            'id'        => 'telephone',
            'type'      => 'text',
            'title'     => __('Telephone', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'fax_number',
            'type'      => 'text',
            'title'     => __('Fax Number', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'email_address',
            'type'      => 'text',
            'validate'  => 'email',
            'title'     => __('Email Address', 'redux-framework-demo'),
            'subtitle'  => __('Follow the valid email is required.', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'address',
            'type'      => 'text',
            'title'     => __('Address', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'slogan_title_footer',
            'type'      => 'text',
            'title'     => __('Website Slogan Title for Footer', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'slogan_header_footer',
            'type'      => 'text',
            'title'     => __('Website Slogan for Footer', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'contact_btn',
            'type'      => 'text',
            'title'     => __('Contact Button', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'contact_url',
            'type'      => 'text',
            'title'     => __('Contact Button URL', 'redux-framework-demo'),
            'default'   => false
        ),
        array(
            'id'        => 'copyright_text',
            'type'      => 'textarea',
            'title'     => __('Copyright Text', 'redux-framework-demo'),
            'subtitle'  => __('It should be a copyright text.', 'redux-framework-demo'),
            'default'   => ''
        ),
        array(
            'id'        => 'default_thumb',
            'type'      => 'media',
            'title'     => __('Default Thumbnail', 'redux-framework-demo'),
            'subtitle'  => __('Default thumbnail will be used instead of the empty thumbnail. It will be shown in the site if the feature image of the posts is empty. </br>*Note: Default thumbnail should be or bigger than 1920 x 1280', 'redux-framework-demo'),
        ),
        array(
            'id'        => 'default_hero',
            'type'      => 'media',
            'title'     => __('Default Hero Image', 'redux-framework-demo'),
            'subtitle'  => __('Default Hero Image will be used instead of the page within empty hero image. It will be shown at the top of the site (Slider or Hero) if the Hero field or Slick Slider are empty. </br>*Note: Default Hero should be or bigger than 1920 x 1280', 'redux-framework-demo'),
        )
    )
) );

// ---> Custom Code
Redux::setSection( $opt_name, array(
    'icon'      => 'el-icon-glasses',
    'title'     => __('Custom Code', 'redux-framework-demo'),
    'fields'    => array(
        array(
            'id'        => 'custom_css',
            'type'      => 'textarea',
            'title'     => __('Custom CSS', 'redux-framework-demo'),
            'subtitle'  => __('Quickly add some CSS by adding it to this block.', 'redux-framework-demo'),
            'rows'      => 20
        ),
        array(
            'id'        => 'custom_js_header',
            'type'      => 'textarea',
            'title'     => __('Custom JavaScript/Analytics Header', 'redux-framework-demo'),
            'subtitle'  => __('Paste here JavaScript and/or Analytics code wich will appear in the Header of your site. DO NOT include opening and closing script tags.', 'redux-framework-demo'),
            'rows'      => 12
        ),
        array(
            'id'        => 'custom_js_footer',
            'type'      => 'textarea',
            'title'     => __('Custom JavaScript/Analytics Footer', 'redux-framework-demo'),
            'subtitle'  => __('Paste JavaScript and/or Analytics code which will appear in the Footer of your site. DO NOT include opening and closing script tags.', 'redux-framework-demo'),
            'rows'      => 12
        )
    )
) );

/*
 * <--- END DECLARATION OF SECTIONS
 */



// Custom CSS to improve the design of Theme Options
function redux_addPanelCSS() {
    wp_register_style(
        'redux-custom-css',
        get_template_directory_uri().'/admin/css/redux-custom.css',
        array( 'redux-admin-css' ), // Be sure to include redux-admin-css so it's appended after the core css is applied
        time(),
        'all'
    );
    wp_enqueue_style('redux-custom-css');
}
add_action( 'redux/page/redux_option/enqueue', 'redux_addPanelCSS' );


/** remove redux menu under the tools **/
add_action( 'admin_menu', 'remove_redux_menu', 12 );
function remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}


/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */
function compiler_action( $options, $css, $changed_values ) {
    echo '<h1>The compiler hook has run!</h1>';
    echo "<pre>";
    print_r( $changed_values ); // Values that have changed since the last save
    echo "</pre>";
    //print_r($options); //Option values
    //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
}

/**
 * Custom function for the callback validation referenced above
 * */
if ( ! function_exists( 'redux_validate_callback_function' ) ) {
    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error   = false;
        $warning = false;

        //do your validation
        if ( $value == 1 ) {
            $error = true;
            $value = $existing_value;
        } elseif ( $value == 2 ) {
            $warning = true;
            $value   = $existing_value;
        }

        $return['value'] = $value;

        if ( $error == true ) {
            $return['error'] = $field;
            $field['msg']    = 'your custom error message';
        }

        if ( $warning == true ) {
            $return['warning'] = $field;
            $field['msg']      = 'your custom warning message';
        }

        return $return;
    }
}

/**
 * Custom function for the callback referenced above
 */
if ( ! function_exists( 'redux_my_custom_field' ) ) {
    function redux_my_custom_field( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }
}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
function dynamic_section( $sections ) {
    //$sections = array();
    $sections[] = array(
        'title'  => __( 'Section via hook', 'redux-framework-demo' ),
        'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
        'icon'   => 'el el-paper-clip',
        // Leave this as a blank section, no options just some intro text set above.
        'fields' => array()
    );

    return $sections;
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
function change_arguments( $args ) {
    //$args['dev_mode'] = true;

    return $args;
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
function change_defaults( $defaults ) {
    $defaults['str_replace'] = 'Testing filter hook!';

    return $defaults;
}


// Remove the demo link and the notice of integrated demo from the redux-framework plugin
function redux_removeDemoModeLink() { // Be sure to rename this function to something more unique
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
    }
}
add_action('init', 'redux_removeDemoModeLink');

// Remove the demo link and the notice of integrated demo from the redux-framework plugin
function remove_demo() {

    // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        remove_filter( 'plugin_row_meta', array(
            ReduxFrameworkPlugin::instance(),
            'plugin_metalinks'
        ), null, 2 );

        // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
        remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
    }
}
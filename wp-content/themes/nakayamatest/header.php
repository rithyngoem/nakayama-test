<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nakayama-Test
 */

global $redux_option;
?>
<!DOCTYPE html>
    <html <?php language_attributes(); ?>>
        <head>
            <meta charset="<?php bloginfo( 'charset' ); ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
            <link rel="profile" href="https://gmpg.org/xfn/11">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

            <?php wp_head(); ?>
        </head>

        <body <?php body_class(); ?>>
            <header>
                <div class="top-header">
                    <div class="container">
                        <div class="row top-content">
                            <div class="col-md-6">
                                <p><?php echo $redux_option['slogan_title_header']; ?>/p>
                                <p><?php echo $redux_option['slogan_header']; ?></p>

                                <a class="desktop-only" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <?php if ( $redux_option['site_logo']['url'] != null ) : ?>
                                        <img class="logo img-fluid" src="<?php echo $redux_option['site_logo']['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
                                    <?php endif; ?>
                                </a>
                            </div>

                            <div class="col-md-6 text-right desktop-only">
                                <a href="<?php echo $redux_option['contact_info_url']; ?>" class="btn btn-outline-primary question-ask font-weight-bolder"><?php echo $redux_option['contact_info']; ?></a>
                                <p class="tel">TEL: <a href="tel:<?php echo $redux_option['telephone']; ?>"><?php echo $redux_option['telephone']; ?></a></p>
                                <p class="address"><?php echo $redux_option['address']; ?></p>
                            </div>
                        </div>
                    </div>

                    <nav class="navbar navbar-expand-lg navbar-light mobile-only">
                        <div class="container">
                            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <?php if ( $redux_option['site_logo']['url'] != null ) : ?>
                                    <img class="logo img-fluid" src="<?php echo $redux_option['site_logo']['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
                                <?php endif; ?>
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">

                                <?php echo nakayamatest_menu( 'primary-menu' ); ?>

                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="main-menu">
                    <div class="container">
                        <nav class="navbar navbar-expand-sm navbar-light desktop-only">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContentDesktop" aria-controls="navbarSupportedContentDesktop" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContentDesktop">
                                <ul class="navbar-nav justify-content-around">

                                    <?php echo nakayamatest_menu( 'primary-menu' ); ?>

                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>